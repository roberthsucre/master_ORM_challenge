# master_ORM_challenge

Proyecto invidual del bloque de base de datos de dev to hack



Enunciado

Reto bloque II - Master ORM challenge

En la actualidad los ORM como ActiveRecord pueden ayudarnos a manejar data de

manera fácil y rápida. Nos permiten abstraernos de SQL y utilizar programación orientada

a objetos para poder interactuar con nuestra data y representar nuestra lógica de

negocios.

Los modelos es donde pasa la magia, responde a una convención llamada f at

models y es que justo en ellos es donde declaramos como se comporta nuestra aplicación y

cómo interactúan los datos.

Enunciado

En el negocio marítimo existen empresas ( navieras ) que se dedican a transportar

mercancía de un punto a otro alrededor de todo el mundo, esto se hace en grandes

buques con nombres bastante peculiares. Estos buques transportan mercancía en

contenedores que son de diferentes tamaños y tipos; cada contenedor tiene un

identificador único compuesto por una serie de caracteres alfanuméricos, los primeros

cuatro caracteres son letras y los siguientes siete son números.

Los principales tipos de contenedores son ST (Standard), HQ (HIGH-CUBE).

Estos buques pueden cargar en promedio de 2000 a 8000 contenedores , es por

ello que los importadores de los países no necesariamente pueden alquilar un buque

completo para traer su mercancía, así es como entran en juego otras empresas llamadas

NVOCC o consolidadores, cuyo objetivo es agrupar un gran número consignatarios

(clientes finales) para que a las navieras les sea rentable ir de un lugar a otro con sus

buques llenos.

Los NVOCC pueden tener muchos clientes los cuales consiguen dando precios

atractivos para estos, pero los clientes pueden también trabajar con muchos NVOCC ya

que puede que para determinados orígenes obtengan mejores precios.

Con todo ese gran volumen de mercancía flotando por el mar, tiene que haber una

manera de identificar a quién pertenece la mercancía transportada, aquí entra en juego un

documento llamado bill of lading (BL). El BL tiene toda la información necesaria del viajeque están realizando los contenedores, estos BLs son emitidos por las navieras y en él

podemos encontrar información como:

Código del BL

Nombre de la Naviera que transporta

Nombre a quien está consignada la carga

Lista de todos los containers.

Origen de la mercancía

Destino de la mercancía

Fecha de atraque

Código de viaje (este viaje es el que realizar el buque de un origen a destino )

Como si eso no fuera todo existen dos tipos de BL, un BL master que es el contrato

que existe entre la Naviera y el NVOCC pero también existen un BL House que es el

contrato que existe entre un NVOCC y el Cliente Final , quiere decir que de un BL Master

pueden desprenderse varios BL House; el BL House posee información parecida a la del BL

Master.

A muy groso modo esto pasa todos los días alrededor del mundo, es por ello que se

necesita representar esta lógica de negocios en un modelo de datos que cumpla los

conceptos de normalización. Es necesario que la solución tecnológica planteada permita

obtener la siguiente información:

● La representación de los datos de un BL físico.

● Un listado todos los BL por naviera.

● Listado de la cantidad de contenedores por sus tipos.

● Listado del top 10 de países destino con más buques tocando puerto en un periodo

de tiempo.

Consideraciones

No importan cuantas migraciones tenga su proyecto, siempre que todas sean

reversibles.

Aplicar valores por default a las columnas de las tablas y/o constraints como not

null

Garantizar la integridad referencial mediante Foreign Keys

Llenar data de prueba con seed

Garantizar que todos los modelos planteados tengan las validaciones necesarias

para asegurarse que solo data válida sea guardada en la base de datos.

Utilizar al menos una validación condicional coherente a la lógica de negocios

planteada

Utilizar al menos una validación personalizada

Construir métodos de instancia que permita visualizar los textos de error

producidas por las validaciones

Utilizar al menos un callback en la lógica de la aplicación.

Usar callback relacionales donde apliquen

Utilizar data real como nombre de navieras, nombres de nvocc, tipos de

contenedores.

Implementar el tipo de asociaciones necesarias para cumplir con el enunciado del

problema, tener en cuenta conceptos aprendidos en el modelado de datos.

Tener en cuenta que deben existir asociaciones has\_one , belongs\_to , has\_many ( sus

derivados) y asociaciones polimórficas donde apliquen.

Consideraciones de la presentación

El proyecto es estrictamente individual.

Durante la realización del proyecto surgirán dudas, no es recomendable quedarse

con ellas.

Cada estudiante tendrá 10 minutos como máximo para presentar su proyecto.

La promo tendrá que coordinar ANTES del día de la presentación el orden en que

presentarán los proyecto, de no haberse puesto de acuerdo, el tiempo de

presentación se reducirá a 5 minutos por estudiante.

La presentación consistirá en exponer de forma lógica como se estructuró el

proyecto, de la misma manera como interactúa la data entre los modelos y

asociaciones.

No se permite cargar data durante la presentación, la data debe de estar

precargada.

El grupo de mentores evaluará:

 1. Estilo de codificación

 2. Diseño de la solución al problema planteado

 3. Funcionalidad

 4. Cumplimento de los objetivos planteados

