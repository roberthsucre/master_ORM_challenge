# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_21_061039) do

  create_table "bl_houses", force: :cascade do |t|
    t.integer "client_id", null: false
    t.integer "blm_id", null: false
    t.index ["blm_id"], name: "index_bl_houses_on_blm_id"
    t.index ["client_id"], name: "index_bl_houses_on_client_id"
  end

  create_table "bl_masters", force: :cascade do |t|
    t.string "code", null: false
    t.integer "ship_company_id", null: false
    t.integer "ship_id", null: false
    t.integer "nvocc_id", null: false
    t.integer "travel_id", null: false
    t.index ["nvocc_id"], name: "index_bl_masters_on_nvocc_id"
    t.index ["ship_company_id"], name: "index_bl_masters_on_ship_company_id"
    t.index ["ship_id"], name: "index_bl_masters_on_ship_id"
    t.index ["travel_id"], name: "index_bl_masters_on_travel_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "containers", force: :cascade do |t|
    t.string "code", null: false
    t.integer "ship_id", null: false
    t.integer "kind_id", null: false
    t.integer "size_id", null: false
    t.index ["kind_id"], name: "index_containers_on_kind_id"
    t.index ["ship_id"], name: "index_containers_on_ship_id"
    t.index ["size_id"], name: "index_containers_on_size_id"
  end

  create_table "kinds", force: :cascade do |t|
    t.string "kind", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string "continent", null: false
    t.string "country", null: false
    t.string "city", null: false
    t.string "port", null: false
  end

  create_table "nvoc_cs", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "ship_companies", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "ships", force: :cascade do |t|
    t.string "name", null: false
    t.integer "ship_id", null: false
    t.index ["ship_id"], name: "index_ships_on_ship_id"
  end

  create_table "sizes", force: :cascade do |t|
    t.string "size", null: false
  end

  create_table "travels", force: :cascade do |t|
    t.string "code", null: false
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.date "sail_date", null: false
    t.date "landing_date", null: false
    t.index ["destination_id"], name: "index_travels_on_destination_id"
    t.index ["origin_id"], name: "index_travels_on_origin_id"
  end

end
