class CreateTravels < ActiveRecord::Migration[5.2]
  def change
    create_table :travels do |t|
      t.string :code, unique: true, null: false
      t.references :origin, null: false, foreign_key: true
      t.references :destination, null: false, foreign_key: true
      t.date :sail_date, null: false
      t.date :landing_date, null: false
    end
  end
end
