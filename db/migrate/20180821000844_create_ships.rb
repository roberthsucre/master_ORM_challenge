class CreateShips < ActiveRecord::Migration[5.2]
  def change
    create_table :ships do |t|
      t.string :name, null: false
      t.references :ship, null: false, foreign_key: true
    end
  end
end
