class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :continent, null: false
      t.string :country, null: false
      t.string :city, null: false
      t.string :port, null: false
    end
  end
end
