class CreateBlMasters < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_masters do |t|
      t.string :code, unique: true, null: false
      t.references :ship_company, null: false, foreign_key: true
      t.references :ship, null: false, foreign_key: true
      t.references :nvocc, null: false, foreign_key: true
      t.references :travel, null: false, foreign_key: true
    end
  end
end
