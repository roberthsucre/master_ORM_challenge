class CreateShipCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :ship_companies do |t|
      t.string :name, null: false
    end
  end
end
