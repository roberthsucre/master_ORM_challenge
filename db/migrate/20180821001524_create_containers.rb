class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.string :code, unique: true, null: false
      t.references :ship, null: false, foreign_key: true
      t.references :kind, null: false, foreign_key: true
      t.references :size, null: false, foreign_key: true
    end
  end
end
