class CreateBlHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :bl_houses do |t|
      t.references :client, null: false, foreign_key: true
      t.references :blm, null: false, foreign_key: true
    end
  end
end
