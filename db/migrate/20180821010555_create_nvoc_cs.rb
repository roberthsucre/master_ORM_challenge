class CreateNvocCs < ActiveRecord::Migration[5.2]
  def change
    create_table :nvoc_cs do |t|
      t.string :name, null: false
    end
  end
end
